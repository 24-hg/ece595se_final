# for my deep learnig 570 project! # train:augLW valid:HE
from sys import exit
import warnings
from time import time


def print_title(string):
    max_under = 76
    halfstrlen = int(len(string) / 2)
    for _ in range(halfstrlen, int(max_under / 2)):
        print("_", end="")
    print(string, end="")
    for _ in range(halfstrlen, int(max_under / 2)):
        print("_", end="")
    print()


def main():
    import tensorflow as tf
    print_title("checking gpu status")
    from tensorflow.python.client import device_lib
    print(device_lib.list_local_devices())

    ##############################################
    import os
    print_title("perameter define")
    loss_weight_path = "/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/loss_augLW_train_HE_valid.h5"  # point to file
    print(".")
    aac_weight_path = "/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/acc_augLW_train_HE_valid.h5"  # point to file
    print(".")
    model_path = "/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/wow_model_augLW_train_HE_valid.h5"  # point to directory
    weight_path = "/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/wow_weight_augLW_train_HE_valid.h5"  # point to directory

    # model_name = "wow_model.h5"  # set a name
    print(".")
    aac_save_fig_path = "/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/aac_augLW_train_HE_valid.png"  # point to directory

    # aac_save_fig_name = "aac.png"  # set a name
    print(".")
    loss_save_fig_path = "/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/loss_augLW_train_HE_valid.png"  # point to directory
    # print(".")
    # loss_save_fig_name = "loss.png"  # set a name
    print(".")
    learning_rate = 0.001
    print(".")
    while_train_showing_period = 1
    print(".")
    epochs0 = 58
    print(".")
    batchsize = 4
    print(".")
    valid_batchsize = 1

    print(".")
    # train_dir = r"/content/drive/My Drive/ECE570_project/dataset/avg_CKP/train"
    # print(".")
    # valid_dir = r"/content/drive/My Drive/ECE570_project/dataset/avg_CKP/valid"
    train_dir = r"/content/drive/My Drive/ECE570_project/dataset/illu_aug_CKP/all_norm_aug_split/train"
    print(".")
    valid_dir = r"/content/drive/My Drive/ECE570_project/dataset/illu_aug_CKP/HEonly_split/test"
    print(".")
    # for root, dirs, files in os.walk(train_dir, topdown=True):
    #     print("files",files)
    train_list = []
    valid_list = []
    for root, dirs, files in os.walk(train_dir, topdown=True):
        for fname in files:
            train_list.append(fname)
    for root, dirs, files in os.walk(valid_dir, topdown=True):
        for fname in files:
            valid_list.append(fname)
    print(".")
    total_train_data_number = len([fname for fname in train_list if fname[-4:] == r".png"])
    print(".")
    total_valid_data_number = len([fname for fname in valid_list if fname[-4:] == r".png"])
    if total_train_data_number == 0 or total_valid_data_number == 0:
        raise ValueError("It will occur ERROR later!\n Because the total data number is 0\n")

    print("total train:", total_train_data_number, "total valid:", total_valid_data_number)
    print("peramter define completed")
    ##############################################
    from keras.preprocessing.image import ImageDataGenerator
    print_title("produce image data")
    # train_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True,preprocessing_function="gray_to_rgb")
    train_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True)
    print(".");
    # valid_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True,preprocessing_function="gray_to_rgb")
    valid_datagen = ImageDataGenerator(rescale=1. / 255, samplewise_center=True, samplewise_std_normalization=True)
    print(".");
    train_generator = train_datagen.flow_from_directory(train_dir, target_size=(340, 340), batch_size=batchsize,color_mode='grayscale',
                                                        class_mode='categorical')
    print(".");
    valid_generator = valid_datagen.flow_from_directory(valid_dir, target_size=(340, 340), batch_size=valid_batchsize,color_mode='grayscale',
                                                      class_mode='categorical')
    print(".")
    ##############################################
    print_title("from main imported dataset")
    print("skip!")
    # x_train_img=[]
    # y_train_label=[]
    # x_valid_img=[]
    # y_valid_label=[]

    ##############################################
    import numpy as np
    print_title("converting a list of data into an array of data, so no ValueError in the future")
    print("skip!")
    # len_x_train_img=len(x_train_img)
    # len_x_valid_img = len(x_valid_img)
    # x_train_img = np.array(x_train_img)
    # x_valid_img = np.array(x_valid_img)

    ##############################################
    print_title("reshaping images(data)")
    print("skip!!")
    # x_train_img=x_train_img.reshape(-1,340,340,1)
    # x_valid_img=x_valid_img.reshape(-1,340,340,1)
    # x_train_img = x_train_img.reshape(len_x_train_img, 331, 331, 1)
    # x_valid_img = x_valid_img.reshape(len_x_valid_img, 331, 331, 1)

    # x_train_img = x_train_img.reshape(1,40, 101, 1)  # turn to vertical & 100*100*1 --gray scale
    #
    # x_valid_img = x_valid_img.reshape(1,20, 27, 1)  # turn to vertical & 100*100*1 --gray scale

    ##############################################
    # from keras.utils.np_utils import  to_categorical
    ##############################################
    print_title("gathering data")
    # y_train_label=to_categorical(y_train_label)
    # y_valid_label=to_categorical(y_valid_label)
    print("skip!")

    ##############################################

    from keras.models import Sequential
    from keras.layers import Dense, Activation, Flatten, Conv2D, MaxPooling2D, BatchNormalization, Dropout
    from keras.optimizers import Adam

    print_title("start building layer")
    model = Sequential()  # open an empty
    # layer 1
    model.add(Conv2D(32, (5, 5), padding="same", input_shape=(340, 340, 1)))
    # model.add(Conv2D(32, (5, 5), padding="same", input_shape=(340, 340, 3)))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    # layer 2
    model.add(Conv2D(32, (5, 5), padding="same"))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    # max pooling
    model.add(MaxPooling2D(pool_size=(2, 2)))


    # layer 3
    model.add(Conv2D(64, (5, 5), padding="same"))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    # layer 4
    model.add(Conv2D(64, (5, 5), padding="same"))
    model.add(Activation('relu'))
    model.add(BatchNormalization())
    # max pooling
    model.add(MaxPooling2D(pool_size=(2, 2)))

    # flatten, in order to put into Dense layer
    model.add(Flatten())

    # Dense1
    model.add(Dense(64))
    model.add(Activation("relu"))
    model.add(BatchNormalization())

    # dropout to reduce overfit
    model.add(Dropout(0.6))  # suggested in this paper

    # Dense2
    model.add(Dense(64))
    model.add(Activation("relu"))
    model.add(BatchNormalization())

    # dropout to reduce overfit
    model.add(Dropout(0.6))  # suggested in this paper

    # End Dense layer
    model.add(Dense(7))  # 7 emotions
    model.add(Activation("softmax"))
    ##############################################
    # build the neural network
    print_title("start building model -> compile")
    model.compile(loss="mse", optimizer=Adam(lr=learning_rate), metrics=['accuracy'])
    print_title("summary of my CNN")
    model.summary()
    ##############################################
    from keras.callbacks import ModelCheckpoint
    print_title("check point, call back")
    chkpoint1 = ModelCheckpoint(filepath=loss_weight_path,
                                monitor="val_loss",
                                mode="auto",
                                period=while_train_showing_period,  # train how many times show once
                                save_best_only="True"
                                )
    print_title(".\n.\n.\n.\n.\n.")
    chkpoint2 = ModelCheckpoint(filepath=aac_weight_path,
                                monitor="val_acc",
                                mode="auto",
                                period=while_train_showing_period,  # train how many times show once
                                save_best_only="true"
                                )
    print_title(".\n.\n.\n.\n.\n.")
    callbacklist = [chkpoint1, chkpoint2]

    ##############################################
    print_title("start training, compile")
    from math import ceil
    print(ceil(total_train_data_number / batchsize), ceil(total_valid_data_number / valid_batchsize))
    train = model.fit_generator(
        generator=train_generator,
        steps_per_epoch=ceil(total_train_data_number / batchsize),
        epochs=epochs0,  # learn 50 times
        validation_data=valid_generator,
        validation_steps=ceil(total_valid_data_number / valid_batchsize),
        verbose=1,
        callbacks=callbacklist
        # shuffle=False
    )
    """
    possible error:AttributeError: 'ProgbarLogger' object has no attribute 'log_values'
    Reason: batchsize is larger than input data
    """
    ##############################################
    print_title("saving weights -> no architecture")
    model.save_weights(f"{weight_path}")
    print(".\n.\n")
    ##############################################
    print_title("saving model -> has architecture")
    model.save(f"{model_path}")
    print(".\n.\n")
    ##############################################
    print_title("evaluate model")
    _, acc = model.evaluate_generator(valid_generator, steps=len(valid_generator), verbose=1)
    print('valid Accuracy: %.3f' % (acc * 100))
    ##############################################
    import matplotlib.pyplot as plt
    # Plot training & validation accuracy values
    plt.plot(train.history['acc'])
    plt.plot(train.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'valid'], loc='upper left')
    plt.show()
    plt.savefig(aac_save_fig_path)

    # Plot training & validation loss values
    plt.plot(train.history['loss'])
    plt.plot(train.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'valid'], loc='upper left')
    plt.show()
    plt.savefig(loss_save_fig_path)
def _predict():
    print_title("checking gpu status")
    from tensorflow.python.client import device_lib
    print(device_lib.list_local_devices())
    ##############################################
    print_title("perameter define")
    model_path=r"/content/drive/My Drive/ECE570_project/model_and_figs_and_weights/wow_model_augLW_train_HE_valid.h5"

    data_path=r"/content/drive/My Drive/ECE570_project/dataset/jeffe_split/valid"
    ##############################################
    import os

    print_title("totle testing file number calculating")
    predict_list=[]
    for root, dirs, files in os.walk(data_path, topdown=True):
        for fname in files:
            predict_list.append(fname)
    total_predict_data_number = len([fname for fname in predict_list if fname[-4:] == r".png"])

    ##############################################
    print_title("load model")
    from keras.models import load_model
    from keras.preprocessing.image import ImageDataGenerator
    test_datagen=ImageDataGenerator(rescale=1./255,color_mode='grayscale'
                                    )
    test_generator=test_datagen.flow_from_directory(
        data_path,
        target_size=(256,256),
        color_mode="grayscale",
        shuffle=False,
        class_mode="categorical",
        batch_size=1)

    model=load_model(model_path)
    exit()
    result=model.predict_generator(test_generator,steps=total_predict_data_number)
    print("result: ",result)

if __name__ == "__main__":
    start = time()
    main()
    _predict()
    print("time consumed:", round((time() - start) / 60, 2), "min")

