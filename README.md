The code uploaded here is the implementation of paper “An Evaluation of Machine Learning Analysis Tools” (unpublished). The whole code base includes the code for Ariadne, code for Pythia, code for shapeflow and code of dataset. All the codes are based on original implementation of those methods. Besides, code of dataset is summarized based on a variety of TensorFlow program bugs from StackOverflow and GitHub. More details are listed as follows.


#Ariadne
-------------------------------
The files under "Ariadne/monaco-languageclient/" is a pre-installed Monaco editor, it can interact with Tomcat9 server. Monaco is acting as client(editor) while Tomcat9 is acting as server (Ariadne). 

Instructions for Ariadne:

The Tomcat9 is under "Ariadne/Jupyterlab_built/official_website_jupyterlab/ apache-tomcat-9.0.45/".


To start the Tomcat9: Run the file "/bin/startup.bat" under "apache-tomcat-9.0.45/".

The files under "Ariadne/intellij/WALA/" contain a patch to install a customized Language Server Protocol plugin for Ariadne for Pycharm.

In order to replicate the customizedLanguage Server Protocol plugin, we recommend to  use IntellijIDEA:

[1] Click on the plugin.xml file under "Ariadne/intellij/WALA/targets”. This step makes the customized plugin. 
[2] Select option "Prepare Plugin Module 'LSP' " in the build menu on IntellijIDEA. 

The files under "Ariadne/Jupyter notebook/jupyterlab-monaco/" is the installer of Monaco editor as an extension of  Jupyterlab. The installer can be installed with Yarn. 

Under "Ariadne/Jupyterlab_build/ipython-ariadne-extension/", the file "ariadne-extension.ipynb" can be run on Jupyterlab. This *.ipynb file includes a demonstration of how Ariadne interacts with the user when it detects a possible error. 

Under "Dateset/JWsh testcase/TensorFlow-Program-Bugs", this folder contains two datasets provided by original author. In this dataset, it contains buggy code as well as repaired versions to test the performance of our code analysis tools. 

"Ariadne/ML/com.ibm.wala.cast.python.ml" acts like a local server running on Pycharm to support Language Server Protocol to make Ariadne work. 

"Ariadne/Pycharm_wala_ariadne" is the finalization of replication Ariadne on Pycharm. 

"Ariadne/visual studio code" and "Ariadne/vscode_wala"is the finalization of replication Ariadne on Visual Studio Code. "Ariadne/WALA-start" is the original author's pre-installed Ariadne.




#Pythia
-------------------------------
We use the artifact of the original paper which contains the Doop program analysis framework that includes Pythia. The artifact is available free of charge on the Dagstuhl Research Online Publication Server (DROPS).  Files under the folder "Pythia" contain a bzip2 file and a readme file “pythia.md”. The *.bz2 file contains the required Ubuntu with a preinstalled docker to run Pythia. Then there is a 
Running Pythia on a single file


A single file can be analyzed by running the following command:

        doop -a <analysis_sensitivity> -i <input_file> -id <analysis_id> --platform python_2 --single-file-analysis --tensor-shape-analysis --full-tensor-precision

Flags used in the above command explained:

-   `-a`: The context sensitivity of the analysis, available options:
    `context-insensitive`, `1-call-site-sensitive`,
    `1-call-site-sensitive+heap`

-   `-i`: The file or files to be analyzed.

-   `-id`: The analysis id.

-   `–single-file-analysis`: Flag for performing optimizations when the analysis input set is a single file.

-   `–tensor-shape-analysis`: Flag enabling the tensor shape analysis.

-   `–full-tensor-precision`: Flag enabling the precise tensor value abstraction described in section 5.3 of our paper.


#ShapeFlow
-------------------------------
The folder "Shapeflow" contains the files including the replication procedures and the requirement files.

Since the authors open-sourced their great work in GitHub for benefiting the community and for facilitating the review and reproduction of their results, We clone the repository and use the code directly. The code is available at \url{https://github.com/vsahil/Static-Analyser_ML}. 

Here are some instructions and design choices for Shapeflow:
-------------------------------
1. Input of our_Operations class should only be lists, not any other objects to make it iterable
2. Input to the `forward` function of any operation should be the variable (changing) parameters of that operation. The constant ones are not.
3. The asserts involving the variable (changing) parameters (something like `x` or `input_tensor`) of the operation is repeated in its main body and its `forward` function. Other asserts are only in the main body of the operation (something like `axis` or `strides`). The main body ones can catch errors before the session is called and the `forward` functions ones can do it after. 
4. The output of our_Operation is always a Tensor object
5. Placeholders, variables, constants, operations whenever declared are added to the default graph (object of class `our_Graph`). This is later used when a session is called.
All operations on objects of class `our_Operation` (eg. __add__, __sub__, __getitem__) must return an object from the same class, so that it can be traced back to needed placeholders.
6. If the input to a function (API) is just tensors, then we can return tensors, no need to create an Operation object as it is only needed when the input shape is not known. If input is Operation object then we should return Operation objects.



